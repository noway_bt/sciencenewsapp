package com.lab4.sciencenewsapp;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * An {@link ArticleAdapter} knows how to create a list item layout for each article
 * in the data source (a list of {@link Article} objects).
 * <p>
 * These list item layouts will be provided to an adapter view like ListView
 * to be displayed to the user.
 */

public class ArticleAdapter extends ArrayAdapter<Article> {

    public ArticleAdapter(Context context, List<Article> articles) {
        super(context, 0, articles);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if there is an existing list item view (called convertView) that we can reuse,
        // otherwise, if convertView is null, then inflate a new list item layout.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.articles_list_item, parent, false);
        }

        // Find the article at the given position in the list of articles
        Article currentArticle = getItem(position);

        // Find the TextView with view ID section
        TextView sectionView = (TextView) listItemView.findViewById(R.id.section);
        // Display the section name of the current article in that TextView.
        String sectionName = currentArticle.getSection();
        sectionView.setText(sectionName);

        // Set the proper background color on the section.
        // Fetch the background from the TextView, which is a GradientDrawable.
        GradientDrawable sectionRectangle = (GradientDrawable) sectionView.getBackground();
        // Get the appropriate background color based on the current article section.
        int sectionColor = getSectionColor(currentArticle.getSection());
        // Set the color on the section rectangle.
        sectionRectangle.setColor(sectionColor);

        // Find the TextView with view ID author_name.
        TextView authorNameView = (TextView) listItemView.findViewById(R.id.author_name);
        // Display the author name of the current article in that TextView.
        String authorName = currentArticle.getAuthor();
        authorNameView.setText(authorName);

        // Find the TextView with view ID title.
        TextView titleView = (TextView) listItemView.findViewById(R.id.title);
        // Display the title of the article.
        String title = currentArticle.getTitle();
        titleView.setText(title);

        // Create a new Date object from the time in milliseconds.
        String dateAndTime = currentArticle.getDate();


        String[] dateTimeArray = dateAndTime.split("T");
        // Find the TextView with view ID date
        TextView dateView = (TextView) listItemView.findViewById(R.id.date);
        String date = dateTimeArray[0];
        dateView.setText(date);

        // Find the TextView with view ID time
        TextView timeView = (TextView) listItemView.findViewById(R.id.time);
        String time = dateTimeArray[1].replace("Z", "");
        timeView.setText(time);

        // Return the list item view that is now showing the appropriate data
        return listItemView;
    }

    /**
     * Return the color for the section based on the subject of the article.
     *
     * @param section subject of article.
     */
    private int getSectionColor(String section) {
        int colorResourceId;

        switch (section) {
            case "World news":
                colorResourceId = R.color.news;
                break;
            case "Books":
                colorResourceId = R.color.books;
                break;
            case "Opinion":
                colorResourceId = R.color.opinion;
                break;
            case "Environment":
                colorResourceId = R.color.environment;
                break;
            case "Art and design":
                colorResourceId = R.color.art;
                break;
            default:
                colorResourceId = R.color.default_section;
                break;
        }
        return ContextCompat.getColor(getContext(), colorResourceId);
    }
}
